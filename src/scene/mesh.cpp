#include "mesh.h"
#include <tinyobj/tiny_obj_loader.h>
#include <iostream>
#include <QFile>

Mesh::Mesh(GLWidget277 *context)
    : Drawable(context),
      mp_texture(nullptr), mp_bgTexture(nullptr)
{}

void Mesh::createCube(const char *textureFile, const char *bgTextureFile)
{
    // Code that sets up texture data on the GPU
    mp_texture = std::unique_ptr<Texture>(new Texture(context));
    mp_texture->create(textureFile);

    mp_bgTexture = std::unique_ptr<Texture>(new Texture(context));
    mp_bgTexture->create(bgTextureFile);

    // TODO: Create VBO data for positions, normals, UVs, and indices

//    std::vector<glm::vec4> pos {glm::vec4(-2, -2, 0, 1),
//                                glm::vec4(2, -2, 0, 1),
//                                glm::vec4(2, 2, 0, 1),
//                                glm::vec4(-2, 2, 0, 1)};

//    std::vector<glm::vec4> nor {glm::vec4(0, 0, 1, 0),
//                                glm::vec4(0, 0, 1, 0),
//                                glm::vec4(0, 0, 1, 0),
//                                glm::vec4(0, 0, 1, 0)};

//    std::vector<glm::vec2> uvs {glm::vec2(0, 0),
//                                glm::vec2(1, 0),
//                                glm::vec2(1, 1),
//                                glm::vec2(0, 1)};

//    std::vector<GLuint> idx {0, 1, 2, 0, 2, 3};

//    count = 6; // TODO: Set "count" to the number of indices in your index VBO

    std::vector<GLuint> idx;
    std::vector<glm::vec4> pos;
    std::vector<glm::vec4> nor;
    std::vector<glm::vec2> uv;

    //right quad
    idx.push_back(0);idx.push_back(1);idx.push_back(2);
    idx.push_back(0);idx.push_back(2);idx.push_back(3);

    pos.push_back(glm::vec4(1, 1, 1, 1));
    pos.push_back(glm::vec4(-1, 1, 1, 1));
    pos.push_back(glm::vec4(-1, 1, -1, 1));
    pos.push_back(glm::vec4(1, 1, -1, 1));

    glm::vec4 rightQuadNor = glm::normalize(glm::vec4(glm::cross(glm::vec3(pos[2]-pos[0]), glm::vec3(pos[1]-pos[0])), 1));
    nor.push_back(rightQuadNor);
    nor.push_back(rightQuadNor);
    nor.push_back(rightQuadNor);
    nor.push_back(rightQuadNor);

    uv.push_back(glm::vec2(0, 1));
    uv.push_back(glm::vec2(1.f/3, 1));
    uv.push_back(glm::vec2(1.f/3, 0.5));
    uv.push_back(glm::vec2(0, 0.5));

    //left quad
    idx.push_back(4);idx.push_back(5);idx.push_back(6);
    idx.push_back(4);idx.push_back(6);idx.push_back(7);

    pos.push_back(glm::vec4(1, -1, 1, 1));
    pos.push_back(glm::vec4(-1, -1, 1, 1));
    pos.push_back(glm::vec4(-1, -1, -1, 1));
    pos.push_back(glm::vec4(1, -1, -1, 1));

    glm::vec4 leftQuadNor = glm::normalize(glm::vec4(glm::cross(glm::vec3(pos[5]-pos[4]), glm::vec3(pos[6]-pos[4])), 1));
//    glm::vec4 leftQuadNor = glm::normalize(glm::cross(pos[5]-pos[4], pos[6]-pos[4]);
    nor.push_back(leftQuadNor);
    nor.push_back(leftQuadNor);
    nor.push_back(leftQuadNor);
    nor.push_back(leftQuadNor);

    uv.push_back(glm::vec2(1.f/3, 1));
    uv.push_back(glm::vec2(2.f/3, 1));
    uv.push_back(glm::vec2(2.f/3, 0.5));
    uv.push_back(glm::vec2(1.f/3, 0.5));

    //front quad
    idx.push_back(8);idx.push_back(9);idx.push_back(10);
    idx.push_back(8);idx.push_back(10);idx.push_back(11);

    pos.push_back(glm::vec4(1, -1, 1, 1));
    pos.push_back(glm::vec4(1, 1, 1, 1));
    pos.push_back(glm::vec4(1, 1, -1, 1));
    pos.push_back(glm::vec4(1, -1, -1, 1));

    glm::vec4 frontQuadNor = glm::normalize(glm::vec4(glm::cross(glm::vec3(pos[9]-pos[8]), glm::vec3(pos[10]-pos[8])), 1));
//    glm::vec4 frontQuadNor = glm::normalize(glm::cross(pos[9]-pos[8], pos[10]-pos[8]);
    nor.push_back(frontQuadNor);
    nor.push_back(frontQuadNor);
    nor.push_back(frontQuadNor);
    nor.push_back(frontQuadNor);

    uv.push_back(glm::vec2(2.f/3, 1));
    uv.push_back(glm::vec2(1, 1));
    uv.push_back(glm::vec2(1, 0.5));
    uv.push_back(glm::vec2(2.f/3, 0.5));

    //back quad
    idx.push_back(12);idx.push_back(13);idx.push_back(14);
    idx.push_back(12);idx.push_back(14);idx.push_back(15);

    pos.push_back(glm::vec4(-1, -1, 1, 1));
    pos.push_back(glm::vec4(-1, 1, 1, 1));
    pos.push_back(glm::vec4(-1, 1, -1, 1));
    pos.push_back(glm::vec4(-1, -1, -1, 1));

    glm::vec4 backQuadNor = glm::normalize(glm::vec4(glm::cross(glm::vec3(pos[13]-pos[12]), glm::vec3(pos[14]-pos[12])), 1));
//    glm::vec4 backQuadNor = glm::normalize(glm::cross(pos[13]-pos[12], pos[14]-pos[12]);
    nor.push_back(backQuadNor);
    nor.push_back(backQuadNor);
    nor.push_back(backQuadNor);
    nor.push_back(backQuadNor);

    uv.push_back(glm::vec2(0, 0.5));
    uv.push_back(glm::vec2(1.f/3, 0.5));
    uv.push_back(glm::vec2(1.f/3, 0));
    uv.push_back(glm::vec2(0, 0));

    //top quad
    idx.push_back(16);idx.push_back(17);idx.push_back(18);
    idx.push_back(16);idx.push_back(18);idx.push_back(19);

    pos.push_back(glm::vec4(-1, -1, 1, 1));
    pos.push_back(glm::vec4(-1, 1, 1, 1));
    pos.push_back(glm::vec4(1, 1, 1, 1));
    pos.push_back(glm::vec4(1, -1, 1, 1));

    glm::vec4 topQuadNor = glm::normalize(glm::vec4(glm::cross(glm::vec3(pos[17]-pos[16]), glm::vec3(pos[18]-pos[16])), 1));
//    glm::vec4 topQuadNor = glm::normalize(glm::cross(pos[17]-pos[16], pos[18]-pos[16]);
    nor.push_back(topQuadNor);
    nor.push_back(topQuadNor);
    nor.push_back(topQuadNor);
    nor.push_back(topQuadNor);

    uv.push_back(glm::vec2(1.f/3, 0.5));
    uv.push_back(glm::vec2(2.f/3, 0.5));
    uv.push_back(glm::vec2(2.f/3, 0));
    uv.push_back(glm::vec2(1.f/3, 0));

    //bottom quad
    idx.push_back(20);idx.push_back(21);idx.push_back(22);
    idx.push_back(20);idx.push_back(22);idx.push_back(23);

    pos.push_back(glm::vec4(-1, -1, -1, 1));
    pos.push_back(glm::vec4(-1, 1, -1, 1));
    pos.push_back(glm::vec4(1, 1, -1, 1));
    pos.push_back(glm::vec4(1, -1, -1, 1));

    glm::vec4 bottomQuadNor = glm::normalize(glm::vec4(glm::cross(glm::vec3(pos[21]-pos[20]), glm::vec3(pos[22]-pos[20])), 1));
//    glm::vec4 bottomQuadNor = glm::normalize(glm::cross(pos[21]-pos[20], pos[22]-pos[20]);
    nor.push_back(bottomQuadNor);
    nor.push_back(bottomQuadNor);
    nor.push_back(bottomQuadNor);
    nor.push_back(bottomQuadNor);

    uv.push_back(glm::vec2(2.f/3, 0.5));
    uv.push_back(glm::vec2(1, 0.5));
    uv.push_back(glm::vec2(1, 0));
    uv.push_back(glm::vec2(2.f/3, 0));



//    std::vector<glm::vec4> pos {glm::vec4(0, 0, 0, 1),
//                                glm::vec4(0, 1, 0, 1),
//                                glm::vec4(1, 0, 0, 1)};

//    std::vector<glm::vec4> nor {glm::normalize(glm::cross(pos[2]-pos[0], pos[1]-pos[0])),
//                                glm::normalize(glm::cross(pos[2]-pos[0], pos[1]-pos[0])),
//                                glm::normalize(glm::cross(pos[2]-pos[0], pos[1]-pos[0]))};

//    std::vector<glm::vec2> uvs {glm::vec2(0, 0),
//                                glm::vec2(1, 0),
//                                glm::vec2(0, 1)};

//    std::vector<GLuint> idx {2,1,0};

    count = 36; // TODO: Set "count" to the number of indices in your index VBO

    generateIdx();
    context->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
    context->glBufferData(GL_ELEMENT_ARRAY_BUFFER, idx.size() * sizeof(GLuint), idx.data(), GL_STATIC_DRAW);

    generatePos();
    context->glBindBuffer(GL_ARRAY_BUFFER, bufPos);
    context->glBufferData(GL_ARRAY_BUFFER, pos.size() * sizeof(glm::vec4), pos.data(), GL_STATIC_DRAW);

    generateNor();
    context->glBindBuffer(GL_ARRAY_BUFFER, bufNor);
    context->glBufferData(GL_ARRAY_BUFFER, nor.size() * sizeof(glm::vec4), nor.data(), GL_STATIC_DRAW);

    generateUV();
    context->glBindBuffer(GL_ARRAY_BUFFER, bufUV);
    context->glBufferData(GL_ARRAY_BUFFER, uv.size() * sizeof(glm::vec2), uv.data(), GL_STATIC_DRAW);
}

void Mesh::create()
{
    // Does nothing, as we have two separate VBO data
    // creation functions: createFromOBJ, which creates
    // our mesh VBOs from OBJ file data, and createCube,
    // which you will implement.
}

void Mesh::bindTexture() const
{
    mp_texture->bind(0);
}

void Mesh::loadTexture() const
{
    mp_texture->load(0);
}


void Mesh::bindBGTexture() const
{
    mp_bgTexture->bind(2);
}

void Mesh::loadBGTexture() const
{
    mp_bgTexture->load(2);
}

void Mesh::createFromOBJ(const char* filename, const char *textureFile, const char *bgTextureFile)
{
    std::vector<tinyobj::shape_t> shapes; std::vector<tinyobj::material_t> materials;
    std::string errors = tinyobj::QLoadObj(shapes, materials, filename);
    std::cout << errors << std::endl;
    if(errors.size() == 0)
    {
        count = 0;
        //Read the information from the vector of shape_ts
        for(unsigned int i = 0; i < shapes.size(); i++)
        {
            std::vector<float> &positions = shapes[i].mesh.positions;
            std::vector<float> &normals = shapes[i].mesh.normals;
            std::vector<float> &uvs = shapes[i].mesh.texcoords;
            std::vector<unsigned int> &indices = shapes[i].mesh.indices;

            bool normalsExist = normals.size() > 0;
            bool uvsExist = uvs.size() > 0;


            std::vector<GLuint> glIndices;
            for(unsigned int ui : indices)
            {
                glIndices.push_back(ui);
            }
            std::vector<glm::vec4> glPos;
            std::vector<glm::vec4> glNor;
            std::vector<glm::vec2> glUV;

            for(int x = 0; x < positions.size(); x += 3)
            {
                glPos.push_back(glm::vec4(positions[x], positions[x + 1], positions[x + 2], 1.f));
                if(normalsExist)
                {
                    glNor.push_back(glm::vec4(normals[x], normals[x + 1], normals[x + 2], 1.f));
                }
            }

            if(uvsExist)
            {
                for(int x = 0; x < uvs.size(); x += 2)
                {
                    glUV.push_back(glm::vec2(uvs[x], uvs[x + 1]));
                }
            }

            generateIdx();
            context->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
            context->glBufferData(GL_ELEMENT_ARRAY_BUFFER, glIndices.size() * sizeof(GLuint), glIndices.data(), GL_STATIC_DRAW);

            generatePos();
            context->glBindBuffer(GL_ARRAY_BUFFER, bufPos);
            context->glBufferData(GL_ARRAY_BUFFER, glPos.size() * sizeof(glm::vec4), glPos.data(), GL_STATIC_DRAW);

            if(normalsExist)
            {
                generateNor();
                context->glBindBuffer(GL_ARRAY_BUFFER, bufNor);
                context->glBufferData(GL_ARRAY_BUFFER, glNor.size() * sizeof(glm::vec4), glNor.data(), GL_STATIC_DRAW);
            }

            if(uvsExist)
            {
                generateUV();
                context->glBindBuffer(GL_ARRAY_BUFFER, bufUV);
                context->glBufferData(GL_ARRAY_BUFFER, glUV.size() * sizeof(glm::vec2), glUV.data(), GL_STATIC_DRAW);
            }

            count += indices.size();
        }
    }
    else
    {
        //An error loading the OBJ occurred!
        std::cout << errors << std::endl;
    }

    mp_texture = std::unique_ptr<Texture>(new Texture(context));
    mp_texture->create(textureFile);

    mp_bgTexture = std::unique_ptr<Texture>(new Texture(context));
    mp_bgTexture->create(bgTextureFile);
}
