#version 150

in vec2 fs_UV;

out vec3 color;

uniform sampler2D u_RenderedTexture;

void main()
{
    // TODO Homework 5
    vec4 baseColor = texture(u_RenderedTexture, fs_UV);//original color for current pixel
    float grey = 0.21 * baseColor.r + 0.72 * baseColor.g + 0.07 * baseColor.b;//formula for calculating grey
    float illuminance = 3;
    color = (0.6 - distance(fs_UV, vec2(0.5, 0.5))) * illuminance * vec3(grey, grey, grey);
}
