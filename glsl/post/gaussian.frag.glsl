#version 150

#define M_PI 3.1415926535897932384626433832795

in vec2 fs_UV;

out vec3 color;

uniform sampler2D u_RenderedTexture;
uniform int u_Time;
uniform ivec2 u_Dimensions;

void main()
{
    // TODO Homework 5
    float illuminance = 5;
    float sigma = 9.f;
    color = vec3(0, 0, 0);
    for(float x = -5.f; x <= 5.f; x++){
        for(float y = -5.f; y <= 5.f; y++){
            float GaussianWeight = exp(-(x*x + y*y)/(2 * sigma*sigma));
            vec4 neighborColor = texture(u_RenderedTexture, fs_UV+vec2(x/u_Dimensions[0], y/u_Dimensions[1]));

            //current pixel color is an Gaussian interpolation of 11*11 neighbor pixels
            color += GaussianWeight * neighborColor.rgb;
        }
    }

    color /= (2 * M_PI * sigma*sigma);
    color *= illuminance;
}
