#version 150

#define M_PI 3.1415926535897932384626433832795

in vec2 fs_UV;

out vec3 color;

uniform sampler2D u_RenderedTexture;
uniform int u_Time;
uniform ivec2 u_Dimensions;

void main()
{
    // TODO Homework 5
    vec4 baseColor = texture(u_RenderedTexture, fs_UV);
    color = baseColor.rgb;
    float threshold = 0.4;
    float sigma = 9.f;
    float coefficient = 2 * M_PI * sigma*sigma / 5;
    for(float x = -5.f; x <= 5.f; x++){
        for(float y = -5.f; y <= 5.f; y++){
            vec4 neighborColor = texture(u_RenderedTexture, fs_UV+vec2(x/u_Dimensions[0], y/u_Dimensions[1]));
            float grey = 0.21 * neighborColor.r + 0.72 * neighborColor.g + 0.07 * neighborColor.b;
            if(grey < threshold) continue;
            float GaussianWeight = exp(-(x*x + y*y)/(2 * sigma*sigma)) / coefficient;

            //current color is a Gaussian interpolation of 11*11 neighbor colors if the neighbor color is above a threshold
            color += GaussianWeight * neighborColor.rgb;
        }
    }
}
