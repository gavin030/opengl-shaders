#version 150

in vec2 fs_UV;

out vec3 color;

uniform sampler2D u_RenderedTexture;
uniform int u_Time;
uniform ivec2 u_Dimensions;

void main()
{
    // TODO Homework 5
    //gradient matrices in x and y direction
    mat3 horizontal = mat3(3, 0, -3,
                           10, 0, -10,
                           3, 0, -3);
    mat3 vertical = mat3(3, 10, 3,
                         0, 0, 0,
                         -3, -10, -3);
    vec3 xCoords = vec3(-1.f/u_Dimensions[0], 0, 1.f/u_Dimensions[0]);
    vec3 yCoords = vec3(-1.f/u_Dimensions[1], 0, 1.f/u_Dimensions[1]);
    vec3 dx = vec3(0, 0, 0);
    vec3 dy = vec3(0, 0, 0);
    for(int i = 0; i <= 2; i++){
        for(int j = 0; j <= 2; j++){
            vec4 baseColor = texture(u_RenderedTexture, fs_UV+vec2(xCoords[i], yCoords[j]));
            dx[0] += horizontal[i][j] * baseColor.r;
            dy[0] += vertical[i][j] * baseColor.r;
            dx[1] += horizontal[i][j] * baseColor.g;
            dy[1] += vertical[i][j] * baseColor.g;
            dx[2] += horizontal[i][j] * baseColor.b;
            dy[2] += vertical[i][j] * baseColor.b;
        }
    }
    color = vec3(distance(dx[0], dy[0]),
                 distance(dx[1], dy[1]),
                 distance(dx[2], dy[2]));
}
