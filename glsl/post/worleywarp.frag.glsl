#version 150

uniform ivec2 u_Dimensions;
uniform int u_Time;

in vec2 fs_UV;

out vec3 color;

uniform sampler2D u_RenderedTexture;

const int cellSize = 4;

void main()
{
    // TODO Homework 5
    vec2 pixelCoord = vec2(fs_UV[0]*u_Dimensions[0], fs_UV[1]*u_Dimensions[1]);//get current pixel location in pixel space
    vec2 cellID = vec2(ivec2(pixelCoord/cellSize));//cell location for current pixel in cell space
    int minDistance = 100000000;
    //smooth with Worley Noise: traverse neighbor cells, for each cell, get the certain point calculated by Worley Noise, choose
    //                          the point that is the nearest to current pixel, current color is in ratio to this distance
    for(float i = -1; i <= 1; i++){
        for(float j = -1; j <= 1; j++){
            vec2 neighborCellID = cellID + vec2(i, j);//neighbor cell location in cell space
            vec2 neighborPointID = neighborCellID//point location for this neighbor cell in cell space
                    + fract(sin(vec2(dot(neighborCellID,vec2(127.1,311.7)),dot(neighborCellID,vec2(269.5,183.3))))*43758.5453);
            vec2 neighborPointCoord = vec2(ivec2(neighborPointID * cellSize));
            float d = distance(pixelCoord, neighborPointCoord);
            if(d >= minDistance) continue;
            vec4 pointColor = texture(u_RenderedTexture, vec2(neighborPointCoord[0]/u_Dimensions[0], neighborPointCoord[1]/u_Dimensions[1]));
            color = pointColor.rgb/d*10;
        }
    }

}

/*
  cellCoord = fs_UV * numCells;
  cellID = ivec2(cellCoord);
  for i =-1 to 1
    for j = -1 to 1
        neighborID = cellID + (i, j)
        neighborPoint = cellID + rand2(neighborID) * 0.5 + 0.5;
        dist = min(dist, distance(neighborPoint, cellCoord))
*/

//2D Worley noise
//vec2 random2(vec2 p)
//{
//    return fract(sin(vec2(dot(p,vec2(127.1,311.7)),dot(p,vec2(269.5,183.3))))*43758.5453);
//}
