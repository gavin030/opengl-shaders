#version 150

uniform ivec2 u_Dimensions;
uniform int u_Time;

in vec2 fs_UV;

out vec3 color;

uniform sampler2D u_RenderedTexture;

const int cellSize = 4;

void main()
{
    // TODO Homework 5
    vec2 pixelCoord = vec2(fs_UV[0]*u_Dimensions[0], fs_UV[1]*u_Dimensions[1]);//get current pixel location in pixel space
    vec2 cellID = vec2(ivec2(pixelCoord/cellSize));//cell location for current pixel in cell space
    int minDistance = 100000000;
    //smooth with Worley Noise: traverse neighbor cells, for each cell, get the certain point calculated by Worley Noise, choose
    //                          the point that is the nearest to current pixel, current color is in ratio to this distance
    for(float i = -1; i <= 1; i++){
        for(float j = -1; j <= 1; j++){
            vec2 neighborCellID = cellID + vec2(i, j);//neighbor cell location in cell space
            vec2 neighborPointID = neighborCellID//point location for this neighbor cell in cell space
                    + u_Time % (u_Dimensions[0]/2) / (u_Dimensions[0]/20) * fract(sin(vec2(dot(neighborCellID*u_Time,vec2(127.1,311.7)),dot(neighborCellID*u_Time,vec2(269.5,183.3))))*43758.5453);
            vec2 neighborPointCoord = vec2(ivec2(neighborPointID * cellSize));
            float d = distance(pixelCoord, neighborPointCoord);
            if(d >= minDistance) continue;
            vec4 pointColor = texture(u_RenderedTexture, vec2(neighborPointCoord[0]/u_Dimensions[0], neighborPointCoord[1]/u_Dimensions[1]));
            float grey = 0.21 * pointColor.r + 0.72 * pointColor.g + 0.07 * pointColor.b;//formula for calculating grey
            color = pointColor.rgb;//vec3(grey, grey, grey);
        }
        if(distance(pixelCoord[0], u_Dimensions[0]/2.0) <= u_Time % (u_Dimensions[0]/2)) color *= 1.5;
        if(distance(pixelCoord[1], u_Dimensions[1]/2.0) <= u_Time % (u_Dimensions[0]/2)) color *= 1.5;
    }

}
