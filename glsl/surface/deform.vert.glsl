#version 150

uniform mat4 u_Model;
uniform mat3 u_ModelInvTr;
uniform mat4 u_View;
uniform mat4 u_Proj;
uniform vec4 u_Eye;
uniform int u_Time;

in vec4 vs_Pos;
in vec4 vs_Nor;
in vec2 vs_UV;

out vec4 fs_Pos;
out vec4 fs_Nor;
out vec2 fs_UV;
out vec4 fs_LightVec;

void main()
{
    // TODO Homework 4
    fs_Nor = normalize(vec4(u_ModelInvTr * vec3(vs_Nor), 0));
    fs_UV = vs_UV;

    vec4 offsetPos = 0.1 * vs_Pos * vec4(sin(vs_Pos.x * 20.f + float(u_Time *0.125f)), cos(vs_Pos.y * 30.f + float(u_Time *0.1f)), sin(vs_Pos.z * 10.f + float(u_Time *0.5f)), 0);
    vec4 modelposition = u_Model * (vs_Pos + offsetPos);
    fs_Pos = modelposition;

    fs_LightVec = u_Eye - modelposition;

    gl_Position = u_Proj * u_View * modelposition;
}
