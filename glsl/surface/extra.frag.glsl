#version 330
#define M_PI 3.1415926535897932384626433832795

uniform sampler2D u_Texture; // The texture to be read from by this shader
uniform int u_Time;

in vec4 fs_Pos;
in vec4 fs_Nor;
in vec2 fs_UV;
in vec4 fs_LightVec;

layout(location = 0) out vec3 out_Col;

void main()
{
    // TODO Homework 4
    vec3 diffuseColor = texture(u_Texture, fs_UV).rgb;
//    vec3 baseColor = vec3(0, 0, 1);
    vec3 paletteBaseColor = vec3(0.5, 0.5, 0.5);
    float c = 1.f/4;
    float d = 10.f;
    float intensity = clamp(dot(normalize(fs_Nor), normalize(fs_LightVec)), 0, 1);
    out_Col = diffuseColor + paletteBaseColor * cos(2 * M_PI * (c * intensity) + 1.f/16 * u_Time);
}
