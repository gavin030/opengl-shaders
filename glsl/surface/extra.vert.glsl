#version 150

uniform mat4 u_Model;
uniform mat3 u_ModelInvTr;
uniform mat4 u_View;
uniform mat4 u_Proj;
uniform vec4 u_Eye;
uniform int u_Time;

in vec4 vs_Pos;
in vec4 vs_Nor;
in vec2 vs_UV;

out vec4 fs_Pos;
out vec4 fs_Nor;
out vec2 fs_UV;
out vec4 fs_LightVec;

void main()
{
    // TODO Homework 4
    fs_Nor = normalize(vec4(u_ModelInvTr * vec3(vs_Nor), 0));
    fs_UV = vs_UV;

    vec4 offsetPos = 0.1 * vs_Pos * vec4(sin(vs_Pos.x * 20.f + float(u_Time *0.125f)), cos(vs_Pos.y * 30.f + float(u_Time *0.1f)), sin(vs_Pos.z * 10.f + float(u_Time *0.5f)), 0);
//    vec4 interpolatedPos = vec4(mix(vs_Pos.x, sqrt(1 - pow(vs_Pos.z, 2)), 0.5), vs_Pos.y, vs_Pos.z, vs_Pos.w);
    vec4 modelposition = u_Model * (vs_Pos + offsetPos);
//    vec4 modelposition = u_Model * interpolatedPos;
//    vec4 offsetPos = vs_Pos * vec4(sin(vs_Pos.y * 20.f + float(u_Time *0.125f)), 0, 0, 0);
//    fs_Pos = vec3(modelposition);
    fs_Pos = modelposition;

    fs_LightVec = u_Eye - modelposition;

    gl_Position = u_Proj * u_View * modelposition;
}
