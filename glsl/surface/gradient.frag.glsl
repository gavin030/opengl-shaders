#version 330
#define M_PI 3.1415926535897932384626433832795

uniform sampler2D u_Texture; // The texture to be read from by this shader

in vec4 fs_Nor;
in vec4 fs_LightVec;

layout(location = 0) out vec3 out_Col;

void main()
{
    // TODO Homework 4
    vec3 baseColor = vec3(1, 0, 0);
    vec3 paletteBaseColor = vec3(0, 1, 0);
    float c = 5.f/4;
    float d = 10.f;
    float intensity = clamp(dot(normalize(fs_Nor), normalize(fs_LightVec)), 0, 1);
    out_Col = baseColor + paletteBaseColor * cos(2 * M_PI * (c * intensity + d));
}
